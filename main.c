#include <stdio.h>
#include <stdlib.h>

#include "file.h"
void help(){
    printf("No argument specified!");
}

int main (int argc,char *argv[]){
    printf("File lister!\n");
    if(argc==1){
        help();
        getch();
        return EXIT_FAILURE;
    }
    return file(argc, argv);
}
