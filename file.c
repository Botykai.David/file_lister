#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "file.h"



#define MALLOC(size,ptr)\
    do {\
        ptr=malloc(size); \
        if(!ptr) \
            abort();\
    }while(0)

#define FREE(ptr) \
    do{ \
        free(ptr); \
        ptr=NULL; \
    }while(0) \

#define BUFFER_SIZE 1024
#define FILE_MODE "r"

int file(int filecount, char *filename[]){
    for (int i=1; i<filecount; i++) {

if(!filename[i])
    return EXIT_FAILURE;

    FILE *f=fopen(filename[i], FILE_MODE);
if(!f){
    printf(strerror(errno));
    return EXIT_FAILURE;
}

    char *line=NULL;
    MALLOC(BUFFER_SIZE, line);

    while(fgets(line, BUFFER_SIZE, f))
        fputs(line, stdout);
        fputs("\n", stdout);
        fclose(f);
        FREE(line);
    }
        getch();
        return EXIT_SUCCESS;
}
